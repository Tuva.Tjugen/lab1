package rockPaperScissors;

import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

public class RockPaperScissors {
	
	public static void main(String[] args) {
    	/* 	
    	 * The code here does two things:
    	 * It first creates a new RockPaperScissors -object with the
    	 * code `new RockPaperScissors()`. Then it calls the `run()`
    	 * method on the newly created object.
         */
        new RockPaperScissors().run();
    }
    
    
    Scanner sc = new Scanner(System.in);
    int roundCounter = 1;
    int humanScore = 0;
    int computerScore = 0;
    List<String> rpsChoices = Arrays.asList("rock", "paper", "scissors");
    
    public void run() {
        while (true){
            System.out.println("Let's play round " + roundCounter);
            //human choice
            String hc = readInput("Your choice (Rock/Paper/Scissors)?");
            // if valid
            if ( !hc.equals("rock") && !hc.equals("paper") && !hc.equals("scissors")) {
                System.out.println("I do not understand " + hc + ". Could you try again?");
            } else{

            

             //computer choice
                int rand = (int)(Math.random()* 3);
                String cc = "";
                if (rand == 0){
                cc = "rock";
                } else if(rand ==1) {
                cc = "paper";
                } else {
                cc = "Scissors";
                }
                
                //who wins or tie?
                if(hc.equals(cc)) {
                    System.out.println("Human chose " + hc + ", computer chose " + cc + ". It's a tie!");  
                } else if((hc.equals("rock") && cc.equals("scissors")) || (hc.equals("scissors") && cc.equals("paper")) || (hc.equals("paper") && cc.equals("rock"))){
                    System.out.println("Human chose " + hc + ", computer chose " + cc + ". Human wins!");
                    humanScore += 1;
                } else {
                    System.out.println("Human chose " + hc + ", computer chose " + cc + ". Computer wins!");
                    computerScore += 1;
                }

                System.out.println("Score: human " + humanScore + ", computer " + computerScore);

                roundCounter += 1;
                
                String kp = readInput("Do you wish to continue playing? (y/n)?");
                
                
            
                    if (kp.equals("y")){
                        continue;
                    } else if(kp.equals("n")){
                        System.out.println("Bye bye :)");
                        break;
                        
                    } else{
                        System.out.println("I do not understand " + kp + ". Could you try again?");
                    }


                
                
            }
        
        }

    }

    /**
     * Reads input from console with given prompt
     * @param prompt
     * @return string input answer from user
     */
    public String readInput(String prompt) {
        System.out.println(prompt);
        String userInput = sc.next();
        return userInput;
    }

}
